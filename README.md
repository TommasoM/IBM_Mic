25/7

INSTALLING OPENCV
https://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/

INSTALLING PAHO-MQTT
https://www.hivemq.com/blog/mqtt-client-library-paho-python

INSTALLING IBMIOTF
https://github.com/ibm-watson-iot/iot-python

CONFIGURAZIONE IoT PLATFORM
Niente di strano, creare categoria di device e poi il device.
Cambiare il livello di sicurezza in TLS Optional

26/7

Per la visualizzazione di immagini con Python
https://scottontechnology.com/open-image-opencv-python/

30/07
Info su IBMIOTF
https://console.bluemix.net/docs/services/IoT/reference/mqtt/index.html#ref-mqtt

06/08
script explaination:

PUBLISHER
The publisher is the script that has to run on the publishing device (microscopes)

iot_sender.py
This script you have to include in your app have 3 functions
- connect_to_cloud(clientid, username, password, host)
	Connect to the IBM IoT platform and the broker with your device credentials
- disconnect_from_cloud(client)
	Disconnect from the IBM IoT platform
- send_data(client, data_to_send)
	One you are connected to the cloud, you can use this function to send any Json with max size of 131KB
	The function will get the topic by itself according to the Json file name. If "image" is part of the sended file name, the topic will be "image". Same with "area"

APP
This script defines the logic between publishing events and subscribers

app1.py
This version notify just when an image is received, not other kind of data

app2.py
This version of the app use the event topic to send a notification to the subscribers of that specific topic

SUBSCRIBER
The subscriber script is the one installed on the subscribed device
