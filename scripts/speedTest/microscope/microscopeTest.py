#The first thing to do is to import the scipt for the sender
#The iot_sender.py MUST be in the same folder of the script you are using
import iot_sender as sender
#You won't need those two import, it's just for testing
import time
import random
import sys
import csv

#MQTT paramenters
#The parameters for the connection are this one, maybe we will have to change in the
#future but it's something to do just one time
host='kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid='d:kvcvuf:Microscope:Mic1'
username='use-token-auth'
password='rLoqJE7cIOORdyNtTt'

#Here it create the connection with the given parameter
client = sender.connect_to_cloud(clientid, username, password, host)
json_input = sys.argv[1]
sendings = int(sys.argv[2])
outfile = 'big_100.csv'

with open(outfile, mode='w') as outfile:
	writer = csv.writer(outfile, ['image','time'])
	start_time = time.time()
	last_time = time.time()
	for i in range(0,sendings):
		sender.send_data(client, json_input)

		writer.writerow([i, time.time()-last_time])
		last_time = time.time()
		
	total_time = time.time()-start_time
	writer.writerow(['AVERAGE', total_time/sendings])
	writer.writerow(['TOTAL', total_time])
	
	print("It took "+str(total_time)+" to complete sending of "+str(sendings)+" "+json_input);

#Last but not least, the disconnection
#The sender will be connected all the time, so the disconnection has to be made only
#when we are closing the microscope application or when we want to close the connection
sender.disconnect_from_cloud(client)
print("Disconnected");
#This sleep is to let the time to the sender to disconnect before closing the interpreter
time.sleep(1)
