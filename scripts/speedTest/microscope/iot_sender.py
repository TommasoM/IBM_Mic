import sys
import json
import os
import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

imageTopic='iot-2/evt/image/fmt/json'
areaTopic='iot-2/evt/area/fmt/json'
perimTopic='iot-2/evt/perim/fmt/json'

def disconnect_from_cloud(client):
	client.disconnect()
	
def connect_to_cloud(clientid, username, password, host):
	client = mqtt.Client(clientid)
	client.username_pw_set(username, password)
	client.connect(host, 1883, 60)
	client.loop_start()
	return client

def send_data(client, data_to_send):
	with open(data_to_send) as d:
		data = json.load(d)
	
	if "image" in data_to_send:
		print("Sending image");
		#print(str(data))
		pub = client.publish(imageTopic, json.dumps(data), qos=1)
	elif "area" in data_to_send:
		print("Sending area");
		print(str(data))
		pub = client.publish(areaTopic, json.dumps(data), qos=1)
	elif "perimeter" in data_to_send:
		print("Sending perimeter");
		print(str(data))
		pub = client.publish(perimTopic, json.dumps(data), qos=1)
	pub.wait_for_publish()
