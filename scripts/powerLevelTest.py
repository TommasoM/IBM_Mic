#The first thing to do is to import the scipt for the sender
#The iot_sender.py MUST be in the same folder of the script you are using
import iot_sender as sender
#You won't need the following two import, it's just for testing
import time
import random
import sys

#MQTT paramenters
#The parameters for the connection are this one, maybe we will have to change in the
#future but it's something to do just one time
host='kvcvuf.messaging.internetofthings.ibmcloud.com'
clientid='d:kvcvuf:Microscope:Mic1'
username='use-token-auth'
password='rLoqJE7cIOORdyNtTt'

#Here it create the connection with the given parameter
client = sender.connect_to_cloud(clientid, username, password, host)

p = 70

while True:
    p = (p-3+random.randint(0,4))%100
    sender.send_power(client, p)
    if p%4 == 0:
        sender.send_image(client, "image_big.json")
    time.sleep(1)

#Last but not least, the disconnection
#The sender will be connected all the time, so the disconnection has to be made only
#when we are closing the microscope application or when we want to close the connection
sender.disconnect_from_cloud(client)
print("Disconnected");
#This sleep is to let the time to the sender to disconnect before closing the interpreter
time.sleep(1)
