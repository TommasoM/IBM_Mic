# -*- coding: UTF-8 -*-
import sys
import json
import os
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

#When a device publish, it has to say the specific topic to which it want to
#publish to
imageTopic='iot-2/evt/image/fmt/json'
area_histogramTopic='iot-2/evt/area_histogram/fmt/json'

#MQTT default parameters, obviously not working
#As soon as you try to connect, the create_and_connect_default_client will try
#to read the "config_sender.txt" configuration file in this folder
host_default='host_default'
clientid_default='clientid_default'
username_default='username_default'
password_default='password_default'

#This function read the parameters from the "config_sender.txt" and save the
#parameters into global variables
def read_param_from_file():
	global host_default
	global clientid_default
	global username_default
	global password_default
	with open("config_sender.txt") as f:
	    content = f.readlines()
	# you may also want to remove whitespace characters like `\n` at the end of each line
	content = [x.strip() for x in content]
	host_default = content[0]
	clientid_default = content[1]
	username_default = content[2]
	password_default = content[3]

#This function enstablish a connection with the MQTT broker running on the IoT
#cloud platform and return the already connected client that the device will
#use to publish events
def create_and_connect_client():
	read_param_from_file()
	client = mqtt.Client(clientid_default)
	client.username_pw_set(username_default, password_default)
	client.connect(host_default, 1883, 60)
	client.loop_start()
	return client

#Disconnect the client from the MQTT broker
def disconnect_client(client):
	client.disconnect()

#Send an histogram to the broker. Need 3 parameters:
# - The client
# - A dictionary representing the histogram, with bin number as labels:
#    "0":321
#    "1":210
#    "2":127
#    ....
# - A string defining the type of the histogram. Actually there is only one
#   hystogram type: "area_histogram"
#
#The dictionary can be converted into a JSON very easily and one of the good
#properties of this format is that is possible, anytime, to add new labels and
#information in a very easy way
def send_histogram(client, histogram, hist_type):
	if(hist_type=="area_histogram"):
		print("----- SENDING HISTOGRAM -----")
		pub = client.publish(area_histogramTopic,json.dumps(histogram), qos=0)
	pub.wait_for_publish()

#Send an image to the broker. Need 2 parameters:
# - The client
# - A dictionary representing the image:
#    "image":"<image converted into ASCII>"
#    "area":<the creature area>
#    "resize":<the numer of times the images has been resized"
def send_image(client, image):
	print("----- SENDING IMAGE -----")
	pub = client.publish(imageTopic, json.dumps(image), qos=1)
	pub.wait_for_publish()
